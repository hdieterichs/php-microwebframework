<?php

namespace Hediet\MicroWebFramework\Http;

class RawResponse extends Response
{
    private $data;
    
    
    /**
     * @var string
     */
    private $contentType;
    
    /**
     * @var string
     */
    private $accessControlAllowOrigin;
    
    public function __construct($data)
    {
        $this->data = $data;
    }
    
    /**
     * @param string $value
     */
    public function setAccessControlAllowOrigin($value)
    {
        $this->accessControlAllowOrigin = $value;
    }
    
    /**
     * 
     * @param string $contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }
    
    public function load()
    {
        if ($this->contentType !== null)
            header("Content-type: " . $this->contentType);
        
        if ($this->accessControlAllowOrigin !== null)
            header("Access-Control-Allow-Origin: " . $this->accessControlAllowOrigin);
        
        echo $this->data;
    }
}
