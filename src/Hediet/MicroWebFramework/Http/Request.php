<?php

namespace Hediet\MicroWebFramework\Http;

class Request
{
    public function getVerb()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
    
    public function getRelativePath()
    {
        if (isset($_SERVER['PATH_INFO']))
            return $_SERVER['PATH_INFO'];
        
        return "/";
    }
    
    public function getPath()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    public function getGetVar($name)
    {
        return isset($_GET[$name]) ? $_GET[$name] : null;
    }

    public function getPostVar($name)
    {
        return isset($_POST[$name]) ? $_POST[$name] : null;
    }

}
