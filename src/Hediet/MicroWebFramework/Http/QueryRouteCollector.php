<?php

namespace Hediet\MicroWebFramework\Http;


abstract class QueryRouteCollector
{
    public abstract function add($controllerName, $controller);
}