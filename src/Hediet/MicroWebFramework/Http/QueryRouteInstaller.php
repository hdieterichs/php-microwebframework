<?php

namespace Hediet\MicroWebFramework\Http;

abstract class QueryRouteInstaller
{
    public abstract function install(QueryRouteCollector $collector);
}
