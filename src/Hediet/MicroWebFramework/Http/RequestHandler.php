<?php

namespace Hediet\MicroWebFramework\Http;

interface RequestHandler
{
    /**
     * @param Request $request
     * @return Response
     */
    function handle(Request $request);
}