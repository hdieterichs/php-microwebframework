<?php

namespace Hediet\MicroWebFramework\CrmPlugin;

use Hediet\MicroWebFramework\Http\Request;
use Hediet\MicroWebFramework\Templates\Template;

class ExceptionInterceptor implements RequestHandler
{

    /**
     * @var Template
     */
    private $errorTemplate;

    /**
     * @var array
     */
    private $services;
    private $controller;

    public function __construct(array $services, Template $errorTemplate, 
            $controller)
    {
        parent::__construct($services);

        \Nunzion\Expect::that($controller)->isNotNull();
        $this->controller = $controller;
        $this->errorTemplate = $errorTemplate;
    }

    public function handle(Request $request)
    {
        try
        {
            return $this->controller->handle($request);
        }
        catch (\Exception $e)
        {
            return $this->renderTemplate();
        }
    }

}
