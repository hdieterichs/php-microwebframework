<?php

namespace Hediet\MicroWebFramework\Http;

class PageNotFoundRequestHandler implements RequestHandler
{
    public function handle(Request $request)
    {
        return new RawResponse("Page not found TODO: Send 404 header");
    }
}
