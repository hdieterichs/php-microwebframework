<?php

namespace Hediet\MicroWebFramework\Http;

class QueryRouter extends Router
{
    private $controllerToClassnameMap;
    private $classNameToControllerMap;
    private $url;
    
    private $routeInstaller;
    
    /**
     * 
     * @param string $url the url pointing to the dispatcher without query arguments.
     */
    public function __construct($url, QueryRouteInstaller $routeInstaller)
    {
        $this->controllerToClassnameMap = array();
        $this->classNameToControllerMap = array();
        $this->url = $url;
        $this->routeInstaller = $routeInstaller;
    }
    
    public function addController($className, $controllerName)
    {

    }
    
    public function getPath(PathRouter_Route $route)
    {   
        if (!isset($this->classNameToControllerMap[$route->getController()]))
        {
            return null;
        }
        
        $controller = $this->classNameToControllerMap[$route->getController()];
        
        $args = $route->getArguments();
        
        if (isset($args["path"])) 
        {
            throw new Exception("Escaping of path arguments is not implemented.");
        }
        
        if ($controller != "")
            $queryArgs = array_merge(array("path" => $controller), $args);
        else
            $queryArgs = $args;
        
        $query = http_build_query($queryArgs);
        
        if ($query != "")
            return $this->url . "?" . $query;
        
        return $this->url;
    }

    public function getRoute($path)
    {
        $query = parse_url($path, PHP_URL_QUERY);
        $args = array();
        parse_str($query, $args);
        
        if (isset($args["path"]))
            $path = $args["path"];
        else
            $path = "";

        if (!isset($this->controllerToClassnameMap[$path]))
        {
            return null;
        }
        
        return new Route($this->controllerToClassnameMap[$path], $args);
    }
}

class QueryRouter_QueryRouteCollector extends QueryRouteCollector
{
    public $controllerToClassnameMap;
    public $classNameToControllerMap;
    
    public function add($controllerName, $controller)
    {
        $this->controllerToClassnameMap[$controllerName] = $controller;
        $this->classNameToControllerMap[$className] = $controllerName;
    }
}