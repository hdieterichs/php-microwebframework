<?php

namespace Hediet\MicroWebFramework\Http;

use Hediet\MicroWebFramework\Http\Request;

class RequestDispatcher implements RequestHandler
{
    private $router;
    
    public function __construct(Router $router) 
    {
        $this->router = $router;
    }
    
    public function handle(Request $request)
    {
        $route = $this->router->getRoute($request->getPath());
        
        $controller = $route->getController();
        return $controller($request, $route->getArguments());
    }
}