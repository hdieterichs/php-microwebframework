<?php

namespace Hediet\MicroWebFramework\Http;

class RedirectResponse extends Response
{

    private $targetUrl;

    public function __construct($targetUrl)
    {
        \Nunzion\Expect::that($targetUrl)->isString();

        $this->targetUrl = $targetUrl;
    }

    public function load()
    {
        header("Location: " . $this->targetUrl, true, 302);
    }

}
