<?php

namespace Hediet\MicroWebFramework\Http;

abstract class Response
{

    /**
     * Sends the response to the client.
     */
    public abstract function load();
}
