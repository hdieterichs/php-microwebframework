<?php

namespace Hediet\MicroWebFramework;

use Hediet\MicroWebFramework\Http\Request;
use Hediet\MicroWebFramework\Http\RequestHandler;
use Hediet\Contract;

class EntryPoint
{
    /**
     * @var RequestHandler
     */
    private $requestHandler;
    
    public function __construct(RequestHandler $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }
    
    public function __invoke() 
    {
        $request = new Request();
        $response = $this->requestHandler->handle($request);
        Contract::requires($response != null);
        $response->load();
    }
}
