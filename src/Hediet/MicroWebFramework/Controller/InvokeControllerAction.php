<?php

namespace Hediet\MicroWebFramework\Controller;

use Exception;
use Nunzion\Types\Method;

class InvokeControllerAction
{

    /**
     * @var array
     */
    private $arguments;
    private $instance;

    /**
     * @var Method
     */
    private $method;

    /**
     * 
     * @param Method $method (default attribute)
     * @param callable {Func<object>} $instance
     * @param array $arguments
     */
    public function __construct(Method $method, callable $instance = null, array $arguments = null)
    {
        $this->method = $method;
        $this->arguments = $arguments;
        $this->instance = $instance;
    }
    
    public function getArguments()
    {
        return $this->arguments;
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function getMethod()
    {
        return $this->method;
    }
  
    public function __invoke($args)
    {
        if ($this->method->isStatic())
        {
            if ($this->instance !== null)
                throw new Exception("Cannot call a static method on an instance!");
            $instance = null;
        }
        else
        {
            if ($this->instance !== null)
            {
                $f = $this->instance;
                $instance = $f();
            }
            else
                $instance = $this->method->getType()->newInstance();
        }
        
        $mArgs = array();
        
        foreach ($this->method->getParameters() as $p)
        {
            if (isset($args[$p->getName()]))
                $mArgs[] = $args[$p->getName()];
            else
                $mArgs[] = null;
        }
        
        return call_user_func_array(array($instance, $this->method->getName()), $mArgs);
    }
}
