<?php

namespace Hediet\MicroWebFramework\Controller;

use Hediet\MicroWebFramework\Http\RedirectResponse;
use Hediet\MicroWebFramework\Templates\RenderTemplateResponse;
use Hediet\MicroWebFramework\Templates\Template;
use Hediet\MicroWebFramework\Templates\TemplateHelper;
use Nunzion\Expect;

class Controller
{
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @var TemplateHelper
     */
    private $templateHelper;
   
    public function setTemplateHelper(TemplateHelper $templateHelper)
    {
        $this->templateHelper = $templateHelper;
    }
    

    /**
     * Returns a response which redirects to a specified controller.
     * 
     * @param string $controller
     * @param array $args
     * @return RedirectResponse
     */
    protected function redirectCA($controller, $methodName, array $args = array())
    {
        Expect::that($controller)->isString();
        Expect::that($methodName)->isString();
        
        $url = $this->templateHelper->getUrlCA($controller, $methodName, $args);
        
        return new RedirectResponse($url);
    }
    
    protected function redirect($request)
    {
        $url = $this->templateHelper->getUrl($request);
        
        return new RedirectResponse($url);
    }
    
    /**
     * Returns a response which renders a given template.
     * 
     * @param type $initializedTemplate
     * @return RenderTemplateResponse
     */
    protected function renderTemplate(Template $initializedTemplate)
    {
        return new RenderTemplateResponse($initializedTemplate, $this->templateHelper);
    }
}
