<?php

namespace Hediet\MicroWebFramework\Templates;


class AnonymousTemplate implements Template
{

    private $renderFunc;

    public function __construct($renderFunc)
    {
        $this->renderFunc = $renderFunc;
    }

    public function render(TemplateHelper $h)
    {
        $func = $this->renderFunc;
        $func($h);
    }

    public function getClone()
    {
        //this instance is immutable
        return $this;
    }
}
