<?php

namespace Hediet\MicroWebFramework\Templates;

use Hediet\MicroWebFramework\Http\Response;

class RenderTemplateResponse extends Response
{
    private $initializedTemplate;
    private $templateHelper;
    
    public function __construct($initializedTemplate, TemplateHelper $templateHelper)
    {
        \Nunzion\Expect::that($initializedTemplate)->isNotNull();
        
        $this->initializedTemplate = $initializedTemplate;
        $this->templateHelper = $templateHelper;
    }
    
    public function load()
    {
        $this->templateHelper->render($this->initializedTemplate);
    }
}