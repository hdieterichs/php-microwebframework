<?php

namespace Hediet\MicroWebFramework\Templates;

abstract class TemplateImpl implements Template
{
    public abstract function render(TemplateHelper $h);
            
    public function getClone()
    {
        return clone $this;
    }
}