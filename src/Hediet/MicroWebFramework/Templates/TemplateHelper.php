<?php

namespace Hediet\MicroWebFramework\Templates;

use Hediet\MicroWebFramework\Router\ControllerAction;
use Hediet\MicroWebFramework\Router\UrlBuilder;

class TemplateHelper
{

    /**
     * @var UrlBuilder
     */
    private $urlBuilder;
    
    public function __construct(UrlBuilder $urlBuilder = null)
    {
       $this->urlBuilder = $urlBuilder;
    }
    
    public function toHtml($obj)
    {
        return htmlentities($obj);
    }
    
    public function render($obj)
    {
        if ($obj instanceof Template)
        {
            $obj->render($this);
        }
        else
            echo htmlentities($obj); 
    }

    public function getUrl($request)
    {
        return $this->urlBuilder->getUrl($request);
    }
    
    public function getUrlCA($className, $methodName, array $args = array())
    {
        return $this->getUrl(new ControllerAction($className, $methodName, $args));
    }
}