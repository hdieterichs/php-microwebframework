<?php

namespace Hediet\MicroWebFramework\Templates;

interface ContentTemplate extends Template
{
    function setContent($content);
    function getContent();
}