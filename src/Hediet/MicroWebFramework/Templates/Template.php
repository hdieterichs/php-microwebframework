<?php

namespace Hediet\MicroWebFramework\Templates;

interface Template
{
    function render(TemplateHelper $h);
    
    /**
     * @return self
     */
    function getClone();
}