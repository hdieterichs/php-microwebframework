<?php
namespace Hediet\MicroWebFramework\Resources;

class SubResourceProvider implements ResourceProvider
{
    /**
     * @var ResourceProvider
     */
    private $subResourceProvider;
    /**
     * @var string
     */
    private $baseId;
    /**
     * @var ResourceProvider
     */
    private $next;

    /**
     * @param ResourceProvider $subResourceProvider (default attribute)
     * @param string $baseId
     * @param ResourceProvider $next
     */
    public function __construct(ResourceProvider $subResourceProvider = null, $baseId = "", ResourceProvider $next = null)
    {
        $this->subResourceProvider = $subResourceProvider;
        $this->baseId = $baseId;
        $this->next = $next;
    }

    public function provideResources(ResourceCollector $collector, $idStart = "")
    {
        if ($this->subResourceProvider !== null) {
            $l = min(strlen($this->baseId), strlen($idStart));
            if (substr($this->baseId, 0, $l) === substr($idStart, 0, $l) || $idStart === "") {
                if ($idStart === "")
                    $newIdStart = "";
                else
                    $newIdStart = substr($idStart, $l);

                $c = new SubResourceProvider_ResourceCollector($collector, $this->baseId);
                
                $this->subResourceProvider->provideResources($c, $newIdStart);
            }
        }

        if ($this->next !== null)
            $this->next->provideResources($collector, $idStart);
    }
}

class SubResourceProvider_ResourceCollector implements ResourceCollector
{
    private $baseId;

    /**
     * @var ResourceCollector
     */
    private $baseCollector;

    /**
     * 
     * @param ResourceCollector $baseCollector
     * @param string $baseId
     */
    public function __construct(ResourceCollector $baseCollector, $baseId)
    {
        $this->baseCollector = $baseCollector;
        $this->baseId = $baseId;
    }
    
    public function collectResource(DataResource $resource)
    {
        $this->baseCollector->collectResource(new DataResource($this->baseId . $resource->getId(), $resource->getData()));
    }

}