<?php
namespace Hediet\MicroWebFramework\Resources;

use Contruder\Common\ServiceProvider;
use Contruder\Php\Construction\ValueProvider;

class ResourceQuery implements ValueProvider
{
    /**
     * @var string
     */
    private $resourceService;
    /**
     * @var string
     */
    private $startsWith;

    /**
     * @param ResourceService $resourceService (default attribute)
     * @param string $startsWith
     */
    public function __construct(ResourceService $resourceService, $startsWith)
    {
        $this->resourceService = $resourceService;
        $this->startsWith = $startsWith;
    }

    /**
     * @param ServiceProvider $serviceProvider
     * @return string[]
     */
    function provideValue(ServiceProvider $serviceProvider)
    {
        $result = $this->resourceService->listResources($this->startsWith);
        
        $arr = array();
        foreach ($result as $r)
            $arr[] = $this->resourceService->getUrl($r->getId());
        
        return $arr;
    }
}