<?php
namespace Hediet\MicroWebFramework\Resources;

use Nunzion\IO\File;

class FileData implements Data
{
    /**
     * @var File
     */
    private $file;

    /**
     * @param File $file The file (default attribute).
     */
    public function __construct(File $file)
    {
        $this->file = $file;
    }

    /**
     * @return string Gets the content of the file.
     */
    public function getContent()
    {
        return $this->file->getContent();
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }
}