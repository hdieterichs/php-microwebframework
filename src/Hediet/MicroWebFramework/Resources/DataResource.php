<?php
namespace Hediet\MicroWebFramework\Resources;

class DataResource
{
    private $id;
    private $data;

    public function __construct($id, Data $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Data
     */
    public function getData()
    {
        return $this->data;
    }
}