<?php
namespace Hediet\MicroWebFramework\Resources;

use Hediet\MicroWebFramework\Http\RawResponse;
use Hediet\MicroWebFramework\Router\UrlBuilder;

class ResourceService
{
    /**
     * @var UrlBuilder
     */
    private $urlBuilder;

    /**
     * @var ResourceProvider
     */
    private $resources;

    /**
     * @param UrlBuilder $urlBuilder
     * @param ResourceProvider $resources
     */
    public function __construct(UrlBuilder $urlBuilder, ResourceProvider $resources)
    {
        $this->urlBuilder = $urlBuilder;
        $this->resources = $resources;
    }

    /**
     * @param $startsWithId string
     * @return DataResource[]
     */
    public function listResources($startsWithId = "")
    {
        $collector = new _ResourceService_Collector();
        $this->resources->provideResources($collector, $startsWithId);

        return $collector->getResources();
    }

    public function getUrl($resourceId)
    {
        $resourceId = substr($resourceId, 1);
        
        return $this->urlBuilder->getUrl(function(ResourceService $s) use ($resourceId) { $s->handleResourceRequest($resourceId); });
    }


    public function handleResourceRequest($resourceId)
    {
        $resourceId = "/" . $resourceId;
        
        $resources = $this->listResources($resourceId);

        if (count($resources) > 0)
        {
            /* @var $r DataResource */
            $r = $resources[0];
            if ($r->getId() === $resourceId)
            {
                $response = new RawResponse($r->getData()->getContent());
                $response->setContentType("text/css");
                return $response;
            }
        }

        return null;
    }
}

class _ResourceService_Collector implements ResourceCollector
{
    private $resources = array();

    /**
     * @param DataResource $resource
     * @return mixed
     */
    public function collectResource(DataResource $resource)
    {
        $this->resources[] = $resource;
    }

    /**
     * @return DataResource[]
     */
    public function getResources()
    {
        return $this->resources;
    }
}