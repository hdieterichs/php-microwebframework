<?php

namespace Hediet\MicroWebFramework\Resources;

interface Data
{
    /**
     * @return string Gets the data.
     */
    function getContent();
}