<?php

namespace Hediet\MicroWebFramework\Resources;

interface ResourceProvider
{
    function provideResources(ResourceCollector $collector, $idStart = "");
}

