<?php
namespace Hediet\MicroWebFramework\Resources;

use Nunzion\IO\Directory;
use Nunzion\IO\File;
use Nunzion\IO\FileSelector;
use Nunzion\StringHelper;

class FileResourceProvider implements ResourceProvider
{
    /**
     * @var FileSelector
     */
    private $fileSelector;
    /**
     * @var
     */
    private $baseDirectory;
    /**
     * @var
     */
    private $baseId;

    /**
     * @param FileSelector $fileSelector (default attribute)
     * @param Directory $baseDirectory
     * @param string $baseId
     */
    public function __construct(FileSelector $fileSelector, Directory $baseDirectory, $baseId = "")
    {
        $this->fileSelector = $fileSelector;
        $this->baseDirectory = $baseDirectory;
        $this->baseId = $baseId;
    }

    public function provideResources(ResourceCollector $collector, $idStart = "")
    {
        /* @var $file File */
        foreach ($this->fileSelector->getFiles() as $file) 
        {
            $p = $this->baseDirectory->getPath();
            $p = str_replace("\\", "/", $p);
            $fp = $file->getPath();
            $fp = str_replace("\\", "/", $fp);
            if (StringHelper::startsWith($fp, $p)) 
            {
                $id = $this->baseId . substr($fp, strlen($p));
                if (StringHelper::startsWith($id, $idStart))
                {
                    $collector->collectResource(new DataResource($id, new FileData($file)));
                }
            } else
                throw \Exception("File $fp is not in $p");
        }
    }
}

