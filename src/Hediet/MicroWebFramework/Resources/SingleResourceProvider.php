<?php

namespace Hediet\MicroWebFramework\Resources;

use Nunzion\StringHelper;

class SingleResourceProvider implements ResourceProvider
{
    /**
     * @var Data
     */
    private $data;
    /**
     * @var
     */
    private $id;
    /**
     * @var ResourceProvider
     */
    private $next;

    /**
     * @param Data $data The data to provide (default attribute).
     * @param $id string The id of the data.
     */
    public function __construct(Data $data, $id, ResourceProvider $next)
    {
        $this->data = $data;
        $this->id = $id;
        $this->next = $next;
    }

    public function provideResources(ResourceCollector $collector, $idStart = "/")
    {
        if (StringHelper::startsWith($this->id, $idStart))
            $collector->collectResource(new Resource($this->id, $this->data));
        
        if ($this->next !== null)
            $this->next->provideResources($collector, $idStart); 
    }
}

