<?php

namespace Hediet\MicroWebFramework\Resources;

interface ResourceCollector
{
    /**
     * @param DataResource $resource
     * @return mixed
     */
    function collectResource(DataResource $resource);
}