<?php

namespace Hediet\MicroWebFramework\Router;

abstract class Router
{
    public abstract function getActionAndArguments();
    
    /**
     * @return ConcreteRoute
     */
    public abstract function getRouteByPath($path, $verb);
    
    
    /**
     * @return ConcreteRoute
     */
    public abstract function getRouteByAction($action, $verb);
}