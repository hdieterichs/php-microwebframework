<?php

namespace Hediet\MicroWebFramework\Router;

use Nunzion\Types\Method;

class ControllerRoute
{
    /**
     * @var Method
     */
    private $method;

    /**
     *
     * @var string
     */
    private $verb;
    
    /**
     *
     * @var string
     */
    private $path;
    
    /**
     * 
     * @param string $verb (default attribute)
     * @param string $path (default attribute)
     * @param Method $method (default attribute)
     */
    public function __construct($verb, $path, Method $method)
    {
        $this->verb = $verb;
        $this->path = $path;
        $this->method = $method;
    }
    
    /**
     * 
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * 
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }
    
    /**
     * 
     * @return Method
     */
    public function getMethod()
    {
        return $this->method;
    }
}