<?php

namespace Hediet\MicroWebFramework\Router;

use Hediet\MicroWebFramework\Router\RouteProvider;

class SingleRouteProvider implements RouteProvider
{

    private $action;

    /**
     * @var RouteProvider
     */
    private $next;
    private $path;
    private $verb;

    /**
     * 
     * @param string $verb
     * @param string $path
     * @param callable $action
     * @param RouteProvider $next
     */
    public function __construct($verb, $path, $action, RouteProvider $next = null)
    {
        $this->verb = $verb;
        $this->path = $path;
        $this->next = $next;
        $this->action = $action;
    }
    
    public function provideRoutes(RouteCollector $collector)
    {
        $collector->collectRoute($this->verb, $this->path, $this->action);
        
        if ($this->next !== null)
            $this->next->provideRoutes($collector);
    }
}
