<?php

namespace Hediet\MicroWebFramework\Router;

use Exception;
use Hediet\MicroWebFramework\Controller\InvokeControllerAction;
use Hediet\MicroWebFramework\Router\ActionAndArguments;
use Hediet\MicroWebFramework\Router\PathRouter_Route;
use Hediet\MicroWebFramework\Router\PathRouter_RouteCollectorImpl;
use Hediet\MicroWebFramework\Router\PathRouter_TemplatePath;
use Nunzion\StringHelper;

class PathRouter
{

    /**
     * @var RouteProvider
     */
    private $routeProvider;

    public function __construct(RouteProvider $routeProvider = null)
    {
        $this->routeProvider = $routeProvider;
    }

    /**
     * 
     * @return PathRouter_Route[]
     */
    private function getRoutes()
    {
        $collector = new PathRouter_RouteCollectorImpl();
        if ($this->routeProvider !== null)
            $this->routeProvider->provideRoutes($collector);

        return $collector->routes;
    }

    public function getPath($controller, $methodName, array $arguments = array())
    {
        foreach ($this->getRoutes() as $route)
        {
            $a = $route->getAction();
            if ($a instanceof InvokeControllerAction)
            {
                if ($a->getMethod()->getType()->getName() === $controller &&
                        $a->getMethod()->getName() === $methodName)
                {
                    $p = PathRouter_TemplatePath::parse($route->getPath());

                    return $p->insert($arguments);
                }
            }
        }
    }
    
    /**
     * @param string $path
     * @return ActionAndArguments
     */
    public function getActionAndArguments($verb, $path)
    {
        //$verb = StringHelper::capitalize(strtolower($verb));
        $verb = strtolower($verb);

        $args = null;
        $action = null;

        foreach ($this->getRoutes() as $route)
        {
            if (strtolower($route->getVerb()) === $verb)
            {
                $p = PathRouter_TemplatePath::parse($route->getPath());
                $args = $p->match($path);

                if ($args !== null)
                {
                    $action = $route->getAction();
                    break;
                }
            }
        }

        if ($action === null)
            return null; //Route not found

        return new ActionAndArguments($action, $args);
    }

}


class PathRouter_TemplatePath
{

    /**
     * @return PathRouter_TemplatePath
     */
    public static function parse($str)
    {
        $t = new PathRouter_Tokenizer($str);

        $c = $t->readChar();
        if ($c !== "/")
            throw new Exception("Route must start with '/'");

        $segments = array();

        $break = false;
        while (!$break)
        {
            $segment = null;
            $typeName = null;
            $readType = false;
            while (true)
            {
                $c = $t->readChar();

                if ($c === null)
                {
                    $break = true;
                    break;
                }
                else if ($c === "/")
                    break;
                else if ($c === ":")
                {
                    if ($readType)
                        throw Exception("Type already specified");
                    $readType = true;
                }
                else
                {
                    if ($readType)
                        $typeName .= $c;
                    else
                        $segment .= $c;
                }
            }

            if ($segment === null)
                throw Exception("No segment specified");

            if ($typeName === null)
                $segments[] = new PathRouter_FixedSegment($segment);
            else
                $segments[] = new PathRouter_DynamicSegment($segment, $typeName);
        }

        return new PathRouter_TemplatePath($segments);
    }

    private $segments;

    /**
     * @param $segments Segment[]
     */
    public function __construct($segments)
    {
        $this->segments = $segments;
    }

    /**
     * 
     * @param array $arguments
     * @return string
     */
    public function insert(array $arguments)
    {
        $result = "";
        foreach ($this->segments as $s)
        {
            $result .= "/";
            
            if ($s instanceof PathRouter_FixedSegment)
            {
                $result .= $s->getValue();
            }
            else if ($s instanceof PathRouter_DynamicSegment)
            {
                $result .= $arguments[$s->getParameterName()];
            }
        }
        
        return $result;
    }
    
    /**
     * 
     * @param string $path
     * @return array|null
     */
    public function match($path)
    {
        $args = array();

        foreach ($this->segments as $s)
        {
            if (!StringHelper::startsWith($path, "/"))
                return null;

            $path = StringHelper::removeStart($path, "/");

            if ($s instanceof PathRouter_FixedSegment)
            {
                if (!StringHelper::startsWith($path, $s->getValue()))
                    return null;

                $path = StringHelper::removeStart($path, $s->getValue());
            }
            else if ($s instanceof PathRouter_DynamicSegment)
            {
                $n = $s->getParameterName();
                $p = $s->getParameterType();
                if ($p === "all")
                {
                    $args[$n] = $path;
                    $path = "";
                    break;
                }
                
                $parts = explode("/", $path, 2);
                $args[$n] = $parts[0];
                
                if (count($parts) > 1)
                    $path = "/" . $parts[1];
                else
                    $path = "";
            }
        }
        if ($path !== "")
            return null;
        
        return $args;
    }

}

class PathRouter_Tokenizer
{

    private $str;
    private $pos;

    public function __construct($str)
    {
        $this->str = $str;
        $this->pos = 0;
    }

    public function readChar()
    {
        if ($this->pos >= strlen($this->str))
            return null;

        return $this->str[$this->pos++];
    }

}

abstract class PathRouter_Segment
{
    
}

class PathRouter_FixedSegment extends PathRouter_Segment
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

}

class PathRouter_DynamicSegment extends PathRouter_Segment
{

    private $parameterName;
    private $parameterType;

    public function __construct($parameterName, $parameterType)
    {
        $this->parameterName = $parameterName;
        $this->parameterType = $parameterType;
    }

    /**
     * @return string
     */
    public function getParameterName()
    {
        return $this->parameterName;
    }

    /**
     * @return string
     */
    public function getParameterType()
    {
        return $this->parameterType;
    }

}

class PathRouter_Route
{

    private $action;
    private $path;
    private $verb;

    public function __construct($verb, $path, $action)
    {
        $this->verb = $verb;
        $this->path = $path;
        $this->action = $action;
    }

    function getAction()
    {
        return $this->action;
    }

    function getPath()
    {
        return $this->path;
    }

    function getVerb()
    {
        return $this->verb;
    }

}

class PathRouter_RouteCollectorImpl implements RouteCollector
{

    public $routes = array();

    public function collectRoute($verb, $path, $action)
    {
        $this->routes[] = new PathRouter_Route($verb, $path, $action);
    }

}
