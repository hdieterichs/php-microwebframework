<?php

namespace Hediet\MicroWebFramework\Router;

interface RouteCollector
{
    /**
     * 
     * @param string $verb
     * @param string $path
     * @param callable $action
     */
    function collectRoute($verb, $path, $action);
}