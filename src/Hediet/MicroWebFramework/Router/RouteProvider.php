<?php

namespace Hediet\MicroWebFramework\Router;

interface RouteProvider
{
    function provideRoutes(RouteCollector $collector);
} 