<?php

namespace Hediet\MicroWebFramework\Router;

use Closure;
use Nunzion\CodeEmit\LoggedMockupCall;
use Nunzion\CodeEmit\MockupBuilder;
use Nunzion\CodeEmit\MockupCallLogger;
use Nunzion\Types\Type;
use ReflectionFunction;
use ReflectionParameter;

class ClosureHandlerUrlBuilder implements UrlBuilder
{
    /**
     * @var UrlBuilder
     */
    private $next;

    public function __construct(UrlBuilder $next)
    {
        $this->next = $next;
    }
    
    public function getUrl($request)
    {
        if ($request instanceof Closure)
        {
            $f = new ReflectionFunction($request);
            $parameters = $f->getParameters();
            /* @var $p ReflectionParameter */
            $p = $parameters[0];
            $mockupBuilder = new MockupBuilder();
            $className = $p->getClass()->getName();
            $mockupResult = $mockupBuilder->generateMockup(Type::of($className));
            $callReceiver = new MockupCallLogger();
            $m = $mockupResult->createMockup($callReceiver);
            
            $request($m);

            $calls = $callReceiver->getCalls();
            /* @var $call LoggedMockupCall */
            $call = $calls[0];
            
            $request = new ControllerAction($className, $call->getMethodName(), $call->getArguments());
        }
        
        return $this->next->getUrl($request);
    }
}