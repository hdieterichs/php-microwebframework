<?php

namespace Hediet\MicroWebFramework\Router;

use Hediet\MicroWebFramework\Http\Request;
use Hediet\MicroWebFramework\Http\RequestHandler;

class RouterRequestHandler implements RequestHandler
{
    /**
     * @var PathRouter
     */
    private $router;

    /**
     * @var RequestHandler 
     */
    private $next;
    
    public function __construct(PathRouter $router, RequestHandler $next)
    {
        $this->router = $router;
        $this->next = $next;
    }
    
    public function handle(Request $request)
    {
        $a = $this->router->getActionAndArguments($request->getVerb(), $request->getRelativePath());
        
        if ($a !== null)
        {
            $action = $a->getAction();
            //todo: export arguments
            $response = $action($a->getArguments(), $request);

            if ($response !== null)
                return $response;
        }
        
        return $this->next->handle($request);
    }

}
