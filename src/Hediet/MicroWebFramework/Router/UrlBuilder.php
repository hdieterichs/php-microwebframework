<?php

namespace Hediet\MicroWebFramework\Router;

interface UrlBuilder
{
    /**
     * @param mixed $request The request which will be serialized to a url.
     */
    function getUrl($request);
}


