<?php

namespace Hediet\MicroWebFramework\Router;

class RouterUrlBuilder implements UrlBuilder
{
    /**
     * @var PathRouter
     */
    private $router;

    public function __construct(PathRouter $router)
    {
        $this->router = $router;
    }
    
    /*
    $h->getUrlCA(PageController::getClassName(), 
            "handlePageRequest", array("path" => "test"));

    $h->getUrl(new ControllerAction(PageController::getClassName(), 
            "handlePageRequest", array("path" => "test")));
    $h->getUrl(new HandlePageRequest("test"));
    */
    
    public function getUrl($request)
    {
        if ($request instanceof ControllerAction)
        {
            $path = $this->router->getPath(
                    $request->getClassName(), $request->getMethod(), $request->getArguments());
        
            return $_SERVER["SCRIPT_NAME"] . $path;
        }

        throw new \Exception("Request is not supported");
    }
}