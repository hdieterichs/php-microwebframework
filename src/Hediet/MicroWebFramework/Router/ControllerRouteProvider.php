<?php

namespace Hediet\MicroWebFramework\Router;

use Hediet\MicroWebFramework\Controller\InvokeControllerAction;
use Hediet\MicroWebFramework\Router\RouteProvider;

class ControllerRouteProvider implements RouteProvider
{

    /**
     * @var callable
     */
    private $controller;

    /**
     * @var ControllerRoute[]
     */
    private $routes;

    /**
     * @var RouteProvider
     */
    private $next;


    /**
     * 
     * @param callable $controller
     * @param ControllerRoute[] $routes (default attribute)
     * @param RouteProvider $next
     */
    public function __construct(callable $controller, array $routes, RouteProvider $next = null)
    {
        $this->next = $next;
        $this->controller = $controller;
        $this->routes = $routes;
    }
    
    public function provideRoutes(RouteCollector $collector)
    {
        foreach ($this->routes as $route)
        {
            $collector->collectRoute($route->getVerb(), $route->getPath(), 
                    new InvokeControllerAction($route->getMethod(), $this->controller));
        }
        
        if ($this->next !== null)
            $this->next->provideRoutes($collector);
    }
}
