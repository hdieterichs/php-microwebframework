<?php

namespace Hediet\MicroWebFramework\Router;

class ActionAndArguments
{

    private $action;
    private $arguments;

    /**
     * @param mixed $action
     * @param array $arguments
     */
    public function __construct($action, array $arguments = array())
    {
        $this->action = $action;
        $this->arguments = $arguments;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

}
