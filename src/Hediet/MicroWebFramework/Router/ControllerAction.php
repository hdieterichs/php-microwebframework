<?php

namespace Hediet\MicroWebFramework\Router;

class ControllerAction
{
    private $arguments;
    private $method;
    private $className;

    public function __construct($className, $method = null, 
            array $arguments = array())
    {
        $this->className = $className;
        $this->method = $method;
        $this->arguments = $arguments;
    }
    
    /**
     * 
     * @return array
     */
    function getArguments()
    {
        return $this->arguments;
    }

    /**
     * 
     * @return string
     */
    function getMethod()
    {
        return $this->method;
    }

    /**
     * 
     * @return string
     */
    function getClassName()
    {
        return $this->className;
    }
}
