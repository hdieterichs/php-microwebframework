<?php

namespace Hediet\MicroWebFramework\Api;

class ApiMethodResultInfo
{
    private $type;
    private $description;

    function __construct($type, $description)
    {
        $this->type = $type;
        $this->description = $description;
    }

    function getType()
    {
        return $this->type;
    }

    function getDescription()
    {
        return $this->description;
    }
}