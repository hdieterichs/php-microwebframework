<?php

namespace Hediet\MicroWebFramework\Api;

use Hediet\MicroWebFramework\Router\RouteCollector;
use Hediet\MicroWebFramework\Router\RouteProvider;

class ApiRouteProvider implements RouteProvider
{

    /**
     * @var RouteProvider
     */
    private $next;

    /**
     * @var ApiService
     */
    private $apiService;

    public function __construct(ApiService $apiService, RouteProvider $next = null)
    {
        $this->apiService = $apiService;
        $this->next = $next;
    }
    
    public function provideRoutes(RouteCollector $collector)
    {
        $this->apiService->provideRoutes($collector);
        
        if ($this->next != null)
            $this->next->provideRoutes($collector);
    }
}
