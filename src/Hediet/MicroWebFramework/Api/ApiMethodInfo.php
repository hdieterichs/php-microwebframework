<?php

namespace Hediet\MicroWebFramework\Api;

interface ApiMethodInfo
{
    function getDescription();

    /**
     * @return ApiMethodResultInfo
     */
    function getResultInfo();

    /**
     * @return ApiMethodParameterInfo
     */
    function getParameters();
}