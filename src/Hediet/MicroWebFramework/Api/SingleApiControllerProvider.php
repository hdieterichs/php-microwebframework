<?php

namespace Hediet\MicroWebFramework\Api;

use Doctrine\Common\Annotations\AnnotationReader;
use Nunzion\ArrayHelper;
use Nunzion\Types\Type;
use ReflectionClass;

class SingleApiControllerProvider implements ApiMethodProvider
{

    private $next;

    /**
     * @var callable
     */
    private $instance;

    /**
     * @var Type
     */
    private $type;

    public function __construct(Type $type, callable $instance, ApiMethodProvider $next = null)
    {
        $this->type = $type;
        $this->instance = $instance;
        $this->next = $next;
    }

    public function provideApiMethods(ApiMethodCollector $collector)
    {
        //AnnotationRegistry::registerLoader(new Loader());
        new ApiRoute();
        $reader = new AnnotationReader();

        /* @var $reflClass ReflectionClass */
        $reflClass = $this->type->getReflectionClass();

        foreach ($reflClass->getMethods(\ReflectionMethod::IS_PUBLIC) as $m)
        {
            $annotations = $reader->getMethodAnnotations($m);
            
            /* @var $route ApiRoute */
            $route = ArrayHelper::firstOfType($annotations, ApiRoute::class);
            if ($route != null)
            {
                $action = new DelayedTargetAction($this->instance, $m->getName());
                $collector->collectApiMethod(new ApiMethod($action, $route->verb, $route->path, new ClassMethodApiMethodInfo($m)));
            }
        }
        
    }

}


class Loader {
  public function __invoke($name) {
    return true;
  }
}

