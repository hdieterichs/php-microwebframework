<?php

namespace Hediet\MicroWebFramework\Api;

interface ApiMethodProvider
{
    function provideApiMethods(ApiMethodCollector $collector);
}
