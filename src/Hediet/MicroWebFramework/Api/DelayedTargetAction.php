<?php

namespace Hediet\MicroWebFramework\Api;

class DelayedTargetAction
{
    private $targetFunc;
    private $methodName;
    
    public function __construct($targetFunc, $methodName)
    {
        $this->targetFunc = $targetFunc;
        $this->methodName = $methodName;
    }

    public function __invoke()
    {
        $args = func_get_args();
        $f = $this->targetFunc;
        $target = $f();
        $m = $this->methodName;
        
        return call_user_func_array(array($target, $m), $args);
    }
}