<?php

namespace Hediet\MicroWebFramework\Api;

/**
 * @Annotation
 */
class ApiRoute
{
    /**
     * @Enum({"OPTIONS", "GET", "HEAD", "POST", "PUT", "DELETE", "TRACE"})
     */
    public $verb;
    
    /**
     * @var string
     */
    public $path;
    
    /**
     *
     * @var string[]
     */
    public $queryParameters;
    
    /**
     * @Enum({"AUTO", "FORCE_WRAPPED"})
     */
    public $bodyStyle;
    
    /**
     * @Enum({"WRAPPED", "BARE"})
     */
    public $resultStyle;
}