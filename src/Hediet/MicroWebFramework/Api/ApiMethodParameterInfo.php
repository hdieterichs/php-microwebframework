<?php
namespace Hediet\MicroWebFramework\Api;

class ApiMethodParameterInfo
{
    private $type;
    private $description;
    private $name;

    function __construct($type, $description, $name)
    {
        $this->type = $type;
        $this->description = $description;
        $this->name = $name;
    }

    function getType()
    {
        return $this->type;
    }

    function getDescription()
    {
        return $this->description;
    }

    function getName()
    {
        return $this->name;
    }
}