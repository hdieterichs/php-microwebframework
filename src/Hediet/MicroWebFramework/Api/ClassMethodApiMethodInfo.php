<?php

namespace Hediet\MicroWebFramework\Api;

use Nunzion\Types\ReflectionMethod;


class ClassMethodApiMethodInfo implements ApiMethodInfo
{

    /**
     * @var ReflectionMethod
     */
    private $method;

    public function __construct(ReflectionMethod $method)
    {
        $this->method = $method;
    }
    
    public function getDescription()
    {
        return "";
    }

    public function getParameters()
    {
        $result = array();
        foreach ($this->method->getParameters() as $p)
        {
            $type = $p->getRequiredType();
            $name = $p->getName();
            $result[] = new ApiMethodParameterInfo($type, "", $name);
        }
        return $result;
    }

    public function getResultInfo()
    {
        return null;
    }
}