<?php

namespace Hediet\MicroWebFramework\Api;

use Hediet\MicroWebFramework\Http\RawResponse;
use Hediet\MicroWebFramework\Http\Request;
use Hediet\MicroWebFramework\Http\Response;
use Hediet\MicroWebFramework\Router\RouteCollector;
use Nunzion\Types\Type;

class ApiService
{
    /**
     * @var callable
     */
    private $selfReference;

    /**
     * @var ApiMethodProvider
     */
    private $apiMethodProvider;

    public function __construct(callable $selfReference, ApiMethodProvider $apiMethodProvider = null)
    {
        $this->apiMethodProvider = $apiMethodProvider;
        $this->selfReference = $selfReference;
    }
    
    public function provideRoutes(RouteCollector $collector)
    {      
        if ($this->apiMethodProvider != null)
        {
            $this->apiMethodProvider->provideApiMethods(
                new _ApiMethodCollector($collector, $this->selfReference));
        }
    }
    
    /**
     * 
     * @param array $pathArgs
     * @param Request $request
     * @param callable $action
     * @param ApiMethodInfo $actionInfo
     * @return Response
     */
    public function handleApiCall(array $pathArgs, Request $request, 
            callable $action, ApiMethodInfo $actionInfo)
    {
        
        //query args
        //path args
        //body
        
        $jsonBody = null;
        
        $args = array();
        
        if (strtolower($request->getVerb()) != "options")
        {
            if (strtolower($request->getVerb()) != "get")
            {
                $body = file_get_contents('php://input');
                $jsonBody = (array)json_decode($body);

                foreach ($actionInfo->getParameters() as $p)
                {
                    $name = $p->getName();
                    if (isset($pathArgs[$name]))
                        $args[] = $pathArgs[$name];
                    else
                        $args[] = $this->convertJsonObjToType($jsonBody[$name], $p->getType());
                }
            }
            else
            {
                foreach ($actionInfo->getParameters() as $p)
                {
                    $name = $p->getName();
                    if (isset($pathArgs[$name]))
                        $args[] = $pathArgs[$name];
                    else
                        $args[] = $request->getGetVar($name);
                }
            }
            $result = call_user_func_array($action, $args);
            $json = json_encode($result);
        }
        else
        {
            $json = null;
        }
        
        if ($json == "null")
            $json = "{}";
        
        $response = new RawResponse($json);
        $response->setContentType("application/json");
        //$response->setAccessControlAllowOrigin("*");
        
        //header("Access-Control-Allow-Origin: *");
        //header("Access-Control-Allow-Headers: origin, content-type, accept");
        
        return $response;
    }
    
    
    
    private function convertJsonObjToType($obj, Type $targetType)
    {
        if ($targetType->isArray())
        {
            $arrayItemType = $targetType->getGenericTypeArguments()[0];
            
            $result = array();
            foreach ((array)$obj as $key => $item)
            {
                $result[$key] = $this->convertJsonObjToType($item, $arrayItemType);
            }
            return $result;
        }
        else if ($targetType->isClass())
        {            
            $obj = $targetType->getReflectionClass()->newInstance();
            foreach ((array)$obj as $key => $value)
                $obj[$key] = $value;
            return $obj;
        }
        else
            return $obj;
    }
}


class _ApiMethodCollector implements ApiMethodCollector
{
    /**
     * @var RouteCollector
     */
    private $collector;
    
    /**
     *
     * @var callable
     */
    private $apiServiceReference;
    
    public function __construct(RouteCollector $collector, callable $apiServiceReference)
    {
        $this->collector = $collector;
        $this->apiServiceReference = $apiServiceReference;
    }

    
    public function collectApiMethod(ApiMethod $apiMethod)
    {
        $this->collector->collectRoute($apiMethod->getVerb(), $apiMethod->getPath(),
                new ApiMethodAction($apiMethod->getAction(), $apiMethod->getActionInfo(), $this->apiServiceReference));
        //TODO test
        $this->collector->collectRoute("OPTIONS", $apiMethod->getPath(),
                new ApiMethodAction($apiMethod->getAction(), $apiMethod->getActionInfo(), $this->apiServiceReference));
    }
}