<?php
namespace Hediet\MicroWebFramework\Api;

use Hediet\MicroWebFramework\Http\Request;

class ApiMethodAction
{
    private $action;
    private $actionInfo;
    private $apiServiceReference;

    function __construct(callable $action, ApiMethodInfo $actionInfo, callable $apiServiceReference)
    {
        $this->action = $action;
        $this->actionInfo = $actionInfo;
        $this->apiServiceReference = $apiServiceReference;
    }

    
    public function __invoke(array $pathArgs, Request $request)
    {
        $asr = $this->apiServiceReference;
        /* @var $apiService ApiService */
        $apiService = $asr();
        return $apiService->handleApiCall($pathArgs, $request, $this->action, $this->actionInfo);
    }
}