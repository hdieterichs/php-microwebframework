<?php

namespace Hediet\MicroWebFramework\Api;

interface ApiMethodCollector
{
    public function collectApiMethod(ApiMethod $apiMethod);
}
