<?php

namespace Hediet\MicroWebFramework\Api;

class ApiMethod
{
    /**
     * @var callable
     */
    private $action;
    
    /**
     * @var string
     */
    private $verb;
    
    /**
     * @var string
     */
    private $path;

    /**
     * @var ApiMethodInfo 
     */
    private $actionInfo;
    
    public function __construct(callable $action, $verb, $path, ApiMethodInfo $actionInfo)
    {
        $this->action = $action;
        $this->verb = $verb;
        $this->path = $path;
        $this->actionInfo = $actionInfo;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getVerb()
    {
        return $this->verb;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getActionInfo()
    {
        return $this->actionInfo;
    }
}

